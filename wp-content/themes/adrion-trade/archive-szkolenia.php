<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package adrion-trade
 */

get_header(); ?>


<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>
		</div>
	</div>
		<div class="szkolenia_content center container">
				<h2 class="zapytaj">HARMONOGRAM SZKOLEŃ</h2>
				<table cellspacing="0" >
					<tbody>
						<?php
	

			
				$args = array(
'post_type' => 'szkolenia',

);
$the_query = new WP_Query( $args ); 

// The Loop
if ( $the_query->have_posts() ) {

	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
		<tr>
			<td class="fir_td"><?php echo get_field('data'); ?></td>
			<td class="sec_td"><?php echo get_field('miejsce'); ?></td>
			<td class="thir_td"><?php echo get_the_title(); ?></td>
			<td class="fou_td"><a href="<?php echo get_the_permalink(); ?>" class="btn">ZAPISZ SIĘ</a></td>
		</tr>
		<?php
	}

	/* Restore original Post Data */
	wp_reset_postdata();
} else {
	// no posts found
}

	?>
  
					</tbody>
				</table>
				<div class="zapytaj_form">
					<h2 class="zapytaj">ZAPYTAJ O SZKOLENIE DLA CIEBIE</h2>
				<form>
					<input type="text" placeholder="Imię i nazwisko">
					<input type="email" placeholder="Adres  email">
					<div class="select-style" style='background:  url("<?php echo get_template_directory_uri(); ?>/img/arr.jpg") no-repeat 98% 4px;'>
					  <select>
					      <option value="volvo">Wybierz temat porady</option>
					    <option value="volvo">Volvo</option>
					    <option value="saab">Saab</option>
					    <option value="mercedes">Mercedes</option>
					    <option value="audi">Audi</option>
					  </select>
					</div>
					<textarea name="" placeholder="Wiadomość"></textarea>
					<div class="box">
						  <div class="checkbox">
						    <label>
						      <input type="checkbox" />
						      <i class="input-helper"></i>
						      <span>Wyrażam zgodę na przetwarzanie moich danych osobowych.</span>
						    </label>
						  </div>
						  <div class="checkbox">
						    <label>
						      <input type="checkbox" />
						      <i class="input-helper"></i>
						      <span>Wyrażam zgodę na otrzymywanie informacji handlowej.</span>
						    </label>
						  </div>
						
						</div>
						<input class="sub" type="submit" value="WYŚLIJ">
				</form>
			</div>
		</div>
</div>



	</div>

<?php get_footer(); ?>