<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package adrion-trade
 */

?>

<div class="footer">
	<div class="menu-footer">
		<?php
				wp_nav_menu( array( 
				    'theme_location' => 'main-menu', 
				   ) ); 
				?>
	</div>
	<div class="copy center">
		<p>Copyright 2018 ADRION TRADE POLSKA. Wszystkie prawa zastrzeżone.</p>
	</div>
	<div class="logo-footer center">
		<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/logo.png">
	</div>
</div>

</div>
<?php wp_footer(); ?>

</body>
</html>
