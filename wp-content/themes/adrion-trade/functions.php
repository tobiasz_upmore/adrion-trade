<?php
/**
 * adrion-trade functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package adrion-trade
 */

if ( ! function_exists( 'adrion_trade_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function adrion_trade_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on adrion-trade, use a find and replace
		 * to change 'adrion-trade' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'adrion-trade', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'adrion-trade' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'adrion_trade_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'adrion_trade_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function adrion_trade_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'adrion_trade_content_width', 640 );
}
add_action( 'after_setup_theme', 'adrion_trade_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function adrion_trade_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'adrion-trade' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'adrion-trade' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'adrion_trade_widgets_init' );



// include custom jQuery
function shapeSpace_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js');

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');


function wpb_custom_new_menu() {
  register_nav_menu('main-menu',__( 'Menu glowne' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

/**
 * Enqueue scripts and styles.
 */
function adrion_trade_scripts() {
	wp_enqueue_style( 'adrion-trade-style', get_stylesheet_uri() );

	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css',false,'1.1','all');
	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css',false,'1.1','all');



	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array(), '20151215', false );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array(), '20151215', false );
	wp_enqueue_script( 'adrion-trade-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', false );

	wp_enqueue_script( 'adrion-trade-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', false );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'adrion_trade_scripts' );



function textdomain_register_sidebars() {
 
    /* Register the primary sidebar. */
    register_sidebar(
        array(
            'id' => 'shop-sidebar',
            'name' => __( 'Sidebar - sklep', 'textdomain' ),
            'description' => __( 'Sidebar strony sklepu.', 'textdomain' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        )
    );
 
    /* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'textdomain_register_sidebars' );



add_action( 'init', 'custom_taxonomy_Item' );
function custom_taxonomy_Item()  {
$labels = array(
    'name'                       => 'Marki',
    'singular_name'              => 'Marka',
    'menu_name'                  => 'Marki',
    'all_items'                  => 'Wszystkie marki',
    'parent_item'                => 'Parent Item',
    'parent_item_colon'          => 'Parent Item:',
    'new_item_name'              => 'New Item Name',
    'add_new_item'               => 'Add New Item',
    'edit_item'                  => 'Edit Item',
    'update_item'                => 'Update Item',
    'separate_items_with_commas' => 'Separate Item with commas',
    'search_items'               => 'Search Items',
    'add_or_remove_items'        => 'Add or remove Items',
    'choose_from_most_used'      => 'Choose from the most used Items',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'marka', 'product', $args );
register_taxonomy_for_object_type( 'marka', 'product' );
}




//saving role
add_action( 'woocommerce_created_customer', 'update_user_role' );
function update_user_role( $user_id ) {
   $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $_POST['role'] ) );
}


/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Pliki do pobrania', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Plik do pobrania', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Pliki do pobrania', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Plik do pobrania', 'twentythirteen' ),
        'all_items'           => __( 'Wszystkie Pliki do pobrania', 'twentythirteen' ),
        'view_item'           => __( 'View Plik do pobrania', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Plik do pobrania', 'twentythirteen' ),
        'add_new'             => __( 'Dodaj nowy', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Plik do pobrania', 'twentythirteen' ),
        'update_item'         => __( 'Update Plik do pobrania', 'twentythirteen' ),
        'search_items'        => __( 'Search Plik do pobrania', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Pliki do pobrania', 'twentythirteen' ),
        'description'         => __( 'Plik do pobrania news and reviews', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'pliki_do_pobrania', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );




add_action( 'init', 'custom_taxonomy_rodzajPliku' );
function custom_taxonomy_rodzajPliku()  {
$labels = array(
    'name'                       => 'Rodzaje pliku',
    'singular_name'              => 'Rodzaj pliku',
    'menu_name'                  => 'Rodzaje pliku',
    'all_items'                  => 'Wszystkie rodzaje',
    'parent_item'                => 'Parent Item',
    'parent_item_colon'          => 'Parent Item:',
    'new_item_name'              => 'New Item Name',
    'add_new_item'               => 'Dodaj nowy rodzaj',
    'edit_item'                  => 'Edytuj Rodzaj',
    'update_item'                => 'Update Item',
    'separate_items_with_commas' => 'Separate Item with commas',
    'search_items'               => 'Search Items',
    'add_or_remove_items'        => 'Add or remove Items',
    'choose_from_most_used'      => 'Choose from the most used Items',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'rodzajePliku', 'pliki_do_pobrania', $args );
register_taxonomy_for_object_type( 'rodzajePliku', 'pliki_do_pobrania' );
}


/*
 * Step 1. Add Link to My Account menu
 */
add_filter ( 'woocommerce_account_menu_items', 'misha_log_history_link', 40 );
function misha_log_history_link( $menu_links ){
 
	$menu_links = array_slice( $menu_links, 0, 5, true ) 
	+ array( 'materialy' => 'Materiały' )
	+ array_slice( $menu_links, 5, NULL, true );
 
	return $menu_links;
 
}
/*
 * Step 2. Register Permalink Endpoint
 */
add_action( 'init', 'misha_add_endpoint' );
function misha_add_endpoint() {
 
	// WP_Rewrite is my Achilles' heel, so please do not ask me for detailed explanation
	add_rewrite_endpoint( 'materialy', EP_PAGES );
 
}
/*
 * Step 3. Content for the new page in My Account, woocommerce_account_{ENDPOINT NAME}_endpoint
 */
add_action( 'woocommerce_account_materialy_endpoint', 'misha_my_account_endpoint_content' );
function misha_my_account_endpoint_content() {
 
	// of course you can print dynamic content here, one of the most useful functions here is get_current_user_id()
?>
<div class="files_to">
	<h2>MATERIAŁY DO POBRANIA</h2>
<div class="tabs-stage">
	<?php
		$terms = get_terms('rodzajePliku');
		$licz = 1;
		foreach ($terms as $term) {
			
				$args = array(
'post_type' => 'pliki_do_pobrania',
'tax_query' => array(
    array(
    'taxonomy' => 'rodzajePliku',
    'field' => 'term_id',
    'terms' => $term->term_id
     )
  )
);
$the_query = new WP_Query( $args ); 

// The Loop
if ( $the_query->have_posts() ) {
	?>
		
	<?php
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>

		<?php
		$file = get_field('plik');

	
	
			?>
	
       <?php	echo '<li  style=" background: url('.get_template_directory_uri().'/img/pdf.jpg) no-repeat center center; "><a download="'.get_the_title().'" href="'.$file["url"].'">' . get_the_title() . '</a><h2>'.$file["mime_type"].'</h2></li>';


        ?>
   
			<?php
	
		 
		
	
	}
	?>

	<?php
	/* Restore original Post Data */
	wp_reset_postdata();
} else {
	// no posts found
}

		
			$licz++;
		}

	?>
  
	


</div>

</div>

<?php
 
}
/*
 * Step 4
 */
// Go to Settings > Permalinks and just push "Save Changes" button.






function custom_post_type_faqs() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Faqs', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Faq', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Najczęściej zadawane pytania', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent faq', 'twentythirteen' ),
        'all_items'           => __( 'Wszystkie faqs', 'twentythirteen' ),
        'view_item'           => __( 'View faqs', 'twentythirteen' ),
        'add_new_item'        => __( 'Dodaj faq', 'twentythirteen' ),
        'add_new'             => __( 'Dodaj nowy', 'twentythirteen' ),
        'edit_item'           => __( 'Edit faq', 'twentythirteen' ),
        'update_item'         => __( 'Update faqa', 'twentythirteen' ),
        'search_items'        => __( 'Search faq', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Najczęściej zadawane pytania', 'twentythirteen' ),
        'description'         => __( 'Faqs', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'faqs', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type_faqs', 0 );



function custom_post_type_szkolenia() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Szkolenia', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Szkolenie', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Szkolenia', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent szkolenie', 'twentythirteen' ),
        'all_items'           => __( 'Wszystkie szkolenia', 'twentythirteen' ),
        'view_item'           => __( 'View szkolenie', 'twentythirteen' ),
        'add_new_item'        => __( 'Dodaj szkolenie', 'twentythirteen' ),
        'add_new'             => __( 'Dodaj nowy', 'twentythirteen' ),
        'edit_item'           => __( 'Edit szkolenie', 'twentythirteen' ),
        'update_item'         => __( 'Update szkolenie', 'twentythirteen' ),
        'search_items'        => __( 'Search szkolenie', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Szkolenia', 'twentythirteen' ),
        'description'         => __( 'Szkolenia', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'szkolenia', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type_szkolenia', 0 );




function custom_post_type_przedstawiciele() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Przedstawiciele', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Przedstawiciel', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Przedstawiciele handlowi', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Przedstawiciele', 'twentythirteen' ),
        'all_items'           => __( 'Wszyscy Przedstawiciele', 'twentythirteen' ),
        'view_item'           => __( 'Zobacz Przedstawiciela', 'twentythirteen' ),
        'add_new_item'        => __( 'Dodaj Przedstawiciela', 'twentythirteen' ),
        'add_new'             => __( 'Dodaj nowy', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Przedstawiciela', 'twentythirteen' ),
        'update_item'         => __( 'Update szkolenie', 'twentythirteen' ),
        'search_items'        => __( 'Search szkolenie', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Przedstawiciele', 'twentythirteen' ),
        'description'         => __( 'Przedstawiciele', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'przestawiciele', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type_przedstawiciele', 0 );