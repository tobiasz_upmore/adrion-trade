<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package adrion-trade
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- 	wykluczenie sklepu -->
	<?php
		if (is_page('sklep')) {
			echo '<meta name="robots" content="noindex">';
		}
		if (is_page('produkty')) {
			echo '<meta name="robots" content="noindex">';
		}
	?>

<!-- 	wykluczenie sklepu -->

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >

	<div class="outer">

<div class="container-almost-width pos-abs menu-desk">
	<div class="header-main container-almost-width ">
		<div class="logo">
			<a href="<?php echo home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png">
		</a>
		</div>
		<div class="main-menu">
			<?php
				wp_nav_menu( array( 
				    'theme_location' => 'main-menu', 
				    'container_class' => 'main-menu' ) ); 
				?>
		</div>
	</div>
</div>


	<div class="logo-mobile">
			<a href="<?php echo home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png">
		</a>
		</div>
		<div class="ham">
			<img src="<?php echo get_template_directory_uri(); ?>/img/ham.png">
		</div>
			<div class="clos">
			<img src="<?php echo get_template_directory_uri(); ?>/img/close.png">
		</div>

<div class="menu-mobile pos-abs">

	
	
		<div class="main-menu-mobile">
			<?php
				wp_nav_menu( array( 
				    'theme_location' => 'main-menu', 
				    'container_class' => 'main-menu' ) ); 
				?>
		</div>

</div>



<div style="clear:both;"></div>