<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package adrion-trade
 */

get_header();
?>
<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>

		</div>
	</div>
	<div id="primary" class="content-area">
		<div class="container sklep">
			<div class="sid-sklep"><h2>BE PURE</h2>
					<?php dynamic_sidebar( 'shop-sidebar' ); ?>
			</div>

			<div class="res-sklep">
					<main id="main" class="site-main">

	<?php echo do_shortcode('[woof_products per_page=9 columns=3 is_ajax=1 ]'); ?>

		</main><!-- #main -->
			</div>
	
	</div>
	</div><!-- #primary -->
	<div style="clear:both;"></div>

<?php

get_footer();
