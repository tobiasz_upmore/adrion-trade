<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package adrion-trade
 */

get_header();
?>
<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2>SKLEP</h2>
				
						<a href="<?php echo home_url(); ?>/koszyk">
							<div class="kosz" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/koszyk.jpg) no-repeat center center; ">
								<div class="ops">
									<p class="white">Produktow w koszyku:  <?php echo WC()->cart->get_cart_contents_count(); ?></p>
									<p class="white"><?php echo $woocommerce->cart->get_cart_total();  ?></p>
								</div>
							</div>
						</a>
			
		</div>
	</div>

<div class="container">

<h2 class="center marto"><?php echo get_the_title(); ?></h2>

		<?php
		while ( have_posts() ) :
			the_post();

			wc_get_template_part( 'content', 'single-product' );

			

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		endwhile; // End of the loop.
		?>

	
</div>

<?php

get_footer();
