<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package adrion-trade
 */

get_header();
?>
<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>
				
			
		</div>
	</div>

<div class="container">

<h2 class="center">Zapisujesz się na szkolenie: <?php echo get_the_title(); ?></h2>
<div class="contact_form">
					
				<?php echo do_shortcode('[contact-form-7 id="161" title="Szkolenie"]'); ?>
			</div>
	
</div>

<?php

get_footer();
