<?php
/**
 * Template Name: Strona głowna
 */

get_header(); ?>
<div class="container-almost-width">
	<div class="slid">
		<div class="slider-main">
			<div class="relat">
				<div class="slid-content">
					<h2><?php echo get_field('nalgowek1_slajd1'); ?></h2>
					<p class="con1"><?php echo get_field('naglowek2_slajd1'); ?></p>
					<p class="con2"><?php echo get_field('naglowek3_slajd1'); ?></p>
				</div>
				<img class="img-responsive" src="<?php echo get_field('obrazek_-_slajd_1'); ?>">
			</div>
			<div class="relat">
				<div class="slid-content">
					<h2><?php echo get_field('nalgowek1_slajd2'); ?></h2>
					<p class="con1"><?php echo get_field('naglowek2_slajd2'); ?></p>
					<p class="con2"><?php echo get_field('naglowek3_slajd2'); ?></p>
				</div>
				<img class="img-responsive" src="<?php echo get_field('obrazek_-_slajd_2'); ?>">
			</div>
			<div class="relat">
			<div class="slid-content">
					<h2><?php echo get_field('nalgowek1_slajd3'); ?></h2>
					<p class="con1"><?php echo get_field('naglowek2_slajd3'); ?></p>
					<p class="con2"><?php echo get_field('naglowek3_slajd3'); ?></p>
				</div>
				<img class="img-responsive" src="<?php echo get_field('obrazek_-_slajd_3'); ?>">
			</div>
		</div>

		<div class="slider-nav">
				<div class="slider-nav-div">
					<?php echo get_field('nazwa_nawigacji_-_slajd_1'); ?>
				</div>
				<div class="slider-nav-div">
					<?php echo get_field('nazwa_nawigacji_-_slajd_2'); ?>
				</div>
				<div class="slider-nav-div">
					<?php echo get_field('nazwa_nawigacji_-_slajd_3'); ?>
				</div>
		</div>
	</div>

</div>


	<div class="section2" style=" background: url(<?php echo get_field('obrazek_w_tle_-_sekcja_2'); ?>) no-repeat center center; ">
			<div class="section2_content">
					<?php echo get_field('tresc_-_sekcja_2'); ?>
			</div>
	</div>
	<div class="container">
		<div class="marks">
				<h2 class="center subtitle">POZNAJ NASZE MARKI</h2>
				<div class="row">
					<div class="col25 center">
							<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/all.jpg">
					</div>
								<div class="col25 center">
							<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/black.jpg">
					</div>
								<div class="col25 center">
							<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/vit.jpg">
					</div>
								<div class="col25 center">
							<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/nia.jpg">
					</div>
				</div>
					<div class="row">
						<div class="col33 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/niam.jpg">
						</div>
									<div class="col33 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/dan.jpg">
						</div>
									<div class="col33 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/fio.jpg">
						</div>
							
				</div>
				
		</div>
	</div>
	<div style="clear:both;"></div>
<script>
	$('.slider-main').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 2,
  arrows:false,
asNavFor: '.slider-nav',

});
$('.slider-nav').slick({
	 infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-main',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});
</script>
<?php get_footer(); ?>