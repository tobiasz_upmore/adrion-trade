<?php
/**
 * Template Name: Kontakt
 */

get_header(); ?>


<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>
		</div>
	</div>
		<div class="kontakt_content center container">
			<div class="row">
			<div class="column50">
				<h2 class="zapytaj">SIEDZIBA FIRMY</h2>
				<div class="siedziba">
					<?php echo get_field('siedziba_firmy'); ?>
						
				</div>
				<img class="img-responsive" src="<?php echo get_field('obrazek'); ?>">
			</div>
			<div class="column50">
				<div class="contact_form">
					<h2 class="zapytaj">NAPISZ DO NAS</h2>
					<?php echo do_shortcode('[contact-form-7 id="163" title="Kontakt"]'); ?>
			</div>
			</div>
			<div style="clear:both;"></div>
				<div class="przedstawiciele">
						<h2 class="zapytaj">PRZEDSTAWICIELE HANDLOWI</h2>
							<?php

							$args = array(
							'post_type' => 'przestawiciele',

							);
							$the_query = new WP_Query( $args ); 

							// The Loop
							if ( $the_query->have_posts() ) {

								while ( $the_query->have_posts() ) {
									$the_query->the_post();
									?>	
							 <div class="przedstawiciel column50">
								<div class="column50">
										<img class="img-responsive" src="<?php echo get_field('mapka_-_obraz'); ?>">
								</div>
								<div class="column50 prze_con text-left">
										<?php echo get_field('opis'); ?>
								</div>
							</div>

							      
										<?php
								}

								/* Restore original Post Data */
								wp_reset_postdata();
							} else {
								// no posts found
							}

								?>

				</div>
						<div style="clear:both;"></div>
		</div>
				
		</div>
</div>



	</div>

<?php get_footer(); ?>