<?php
/**
 * Template Name: O nas
 */

get_header(); ?>


<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>
		</div>
	</div>
		<div class="onas_content center">
			<h2>KIM JESTEŚMY?</h2>
			<div class="col40 marauto">
				<p>Adrion Trade Polska to nowoczesna firma, która zajmuje się importem i dystrybucją profesjonalnych kosmetyków fryzjerskich. Szczególną uwagę przykładamy do jakości oferowanych produktów. Współpracujemy z włoskimi producentami, którzy oferują wysoko zaawansowane technologicznie produkty oraz szkolenia na najwyższym światowym poziomie za granicą w kraju i nowo otwartym ośrodku szkoleniowym w Warszawie.   
Posiadamy sieć dystrybucji w całej Polsce zapatrując  ponad 200 hurtowni fryzjerskich. Z wieloma hurtowniami współpracujemy na zasadzie wyłączności dając możliwość indywidualnego rozwoju w kierowaniu marką. Posiadamy również dużą bazę fryzjerów, którzy nam zaufali i poszli razem z nami wspólną drogą. Ciągle poszukujemy nowych rozwiązań dla fryzjerstwa, w nowych produktach, szkoleniach i rozwoju własnym. 
 </p>
			</div>

	<div class="section2" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/onas_img.jpg) no-repeat center center; ">
			<div class="section2_content text-left max30">
				<h2>NASZ ZESPÓŁ</h2>	
				<p>Adrion Trade Polska to zespół profesjonalistów, którzy swoją pasję wykorzystują do ciągłego rozwijania i ulepszania usług firmy. Zespół naszych ekspertów  nieustannie poszukuje i testuje nowe produkty aby zaaferować ekskluzywność, styl i wydajność, która podnosi rangę i opłacalność usług fryzjerskich. 
Nasz zespół doradców handlowych pracujących w terenie dwa razy do roku uczestniczy w specjalistycznych szkoleniach, które mają za zadanie nieustannie podnosić poziom  jakości  współpracy z naszymi klientami.</p>
			</div>
	</div>


			<div class="container">
				<div class="marks">
					<h2 class="center ">PRODUKTY</h2>
					<p>Jesteśmy importerem i wyłącznym dystrybutorem marek: </p>
					<div class="row">
						<div class="col25 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/all.jpg">
						</div>
									<div class="col25 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/black.jpg">
						</div>
									<div class="col25 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/vit.jpg">
						</div>
									<div class="col25 center">
								<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/nia.jpg">
						</div>
					</div>
						<div class="row">
							<div class="col33 center">
									<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/niam.jpg">
							</div>
										<div class="col33 center">
									<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/dan.jpg">
							</div>
										<div class="col33 center">
									<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/fio.jpg">
							</div>
								
					</div>
				
				</div>
			</div>
				<div class="section2" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/motto.jpg) no-repeat center center; ">
			<div class="section2_content2 ">
				<h2>NASZE MOTTO TO</h2>	
				<h2 class="h2big">
					PROFESJONALIŚCI DLA PROFESJONALISTÓW
				</h2>
			
			</div>
	</div>
		</div>
</div>



	</div>

<?php get_footer(); ?>