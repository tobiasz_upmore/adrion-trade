<?php
/**
 * Template Name: Porady
 */

get_header(); ?>


<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>
		</div>
	</div>
		<div class="porady_content center container">
			<h2 class="zapytaj">ZAPYTAJ EKSPERTA</h2>
			<div class="contact_form por_form">
				<?php echo do_shortcode('[contact-form-7 id="168" title="Porady"]'); ?>
			</div>
			
			<h2 class="zapytaj mar_top100">NAJCZĘŚCIEJ ZADAWANE PYTANIA</h2>
			<div class="faqs">
			<?php

								$args = array(
				'post_type' => 'faqs',

				);
				$the_query = new WP_Query( $args ); 

				// The Loop
				if ( $the_query->have_posts() ) {

					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						?>
				<div class="faq">
					<h2><?php echo get_the_title(); ?></h2>
					<p><?php echo get_the_content(); ?></p>
				</div>
						<?php
					}

					/* Restore original Post Data */
					wp_reset_postdata();
				} else {
					// no posts found
				}

					?>
				
			</div>
		</div>
</div>



	</div>

<?php get_footer(); ?>