<?php
/**
 * Template Name: Produkty
 */

get_header(); ?>


<div class="container-almost-width page-def" style=" background: url(<?php echo get_template_directory_uri(); ?>/img/head-ban.jpg) no-repeat center center; ">
		<div class="onas center">
				<h2><?php echo get_the_title(); ?></h2>
		</div>
	</div>

	<div class="container">
	<?php
		$terms = get_terms('marka','orderby=count&hide_empty=0');

		foreach ($terms as $term) {
			// echo $term->name;
		
			?>
				<div class="marka text-left">
					<div class="col30">
						<img src="<?php echo get_field('logo',$term); ?>">
					</div>
					<div class="col70 term_desc">
						<h2><?php echo $term->name; ?></h2>
						<p><?php echo term_description($term); ?></p>
						<a href="<?php echo get_term_link($term); ?>" class="btn btn-prod">ZOBACZ PRODUKTY</a>
					</div>

				</div>
<div style="clear:both;"></div>
			<?php
		}

	?>
	
	</div>

</div>



	</div>

<?php get_footer(); ?>